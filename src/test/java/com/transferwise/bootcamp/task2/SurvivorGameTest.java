package com.transferwise.bootcamp.task2;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by Qwosmic on 23/04/17.
 */
public class SurvivorGameTest {

  SurvivorGame game = new SurvivorGame(10);

  @Test (expected = RuntimeException.class)
  public void testZeroPersons() throws Exception {
    SurvivorGame someGame = new SurvivorGame(0);
  }

  @Test (expected = RuntimeException.class)
  public void testNegativePersons() throws Exception {
    SurvivorGame someGame = new SurvivorGame(-1);
  }

  @Test
  public void testGetNextPosition() throws Exception {
    int p = game.getNextPosition();
    Assert.assertEquals(0, p);

    p = game.getNextPosition();
    Assert.assertEquals(1, p);

    p = game.getNextPosition();
    Assert.assertEquals(3, p);

    p = game.getNextPosition();
    Assert.assertEquals(6, p);
  }

  @Test
  public void testGetNextPerson() throws Exception {
    int p = game.getNextPerson();
    Assert.assertEquals(1, p);

    p = game.getNextPerson();
    Assert.assertEquals(3, p);

    p = game.getNextPerson();
    Assert.assertEquals(6, p);

    p = game.getNextPerson();
    Assert.assertEquals(10, p);
  }

  @Test
  public void testIsActive() throws Exception {
    SurvivorGame game1 = new SurvivorGame(1);
    Assert.assertEquals(false, game1.isActive());

    SurvivorGame game2 = new SurvivorGame(2);
    Assert.assertEquals(true, game2.isActive());

    game2.getNextPerson();
    Assert.assertEquals(false, game2.isActive());
  }

  @Test
  public void testGetSurvivor() throws Exception {
    SurvivorGame game1 = new SurvivorGame(1);
    Assert.assertEquals(1, game1.getSurvivor().intValue());

    SurvivorGame game2 = new SurvivorGame(2);
    Assert.assertEquals(2, game2.getSurvivor().intValue());

    SurvivorGame game3 = new SurvivorGame(3);
    Assert.assertEquals(2, game3.getSurvivor().intValue());
  }

}