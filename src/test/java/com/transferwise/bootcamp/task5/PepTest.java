package com.transferwise.bootcamp.task5;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by Qwosmic on 26/04/17.
 */
public class PepTest {

  @Test
  public void testCreatePep(){
    Pep p = new Pep("John Lennon - England");
    Assert.assertEquals("England", p.contry);
    Assert.assertEquals("John Lennon", p.name);
  }

  @Test (expected = RuntimeException.class)
  public void testCreateIncorrectPep(){
    Pep p = new Pep("John Lennon- England");
  }

  @Test (expected = RuntimeException.class)
  public void testCreateOtherIncorrectPep(){
    Pep p = new Pep("John - Lennon - England");
  }

}