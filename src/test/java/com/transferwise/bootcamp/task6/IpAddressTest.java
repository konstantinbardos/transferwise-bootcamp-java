package com.transferwise.bootcamp.task6;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by Qwosmic on 26/04/17.
 */
public class IpAddressTest {

  @Test
  public void testCreateCorrectIpAdress(){
    IpAddress ip = new IpAddress("1.2.3.4");
    Assert.assertEquals("1.2.3", ip.mask);
  }

  @Test (expected = RuntimeException.class)
  public void testCreateIncorrectIpAdress(){
    IpAddress ip = new IpAddress("1.2.3.4.1");
  }

  @Test (expected = RuntimeException.class)
  public void testCreateOtherIncorrectIpAdress(){
    IpAddress ip = new IpAddress("1.2.34");
  }
}