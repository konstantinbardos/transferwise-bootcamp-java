package com.transferwise.bootcamp;


import com.transferwise.bootcamp.task4.Companies;
import com.transferwise.bootcamp.task4.CurrencyPair;
import com.transferwise.bootcamp.task4.CurrencyPairGenerator;
import com.transferwise.bootcamp.task4.QuoteCollection;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;

/*

Task 4

As a next step, let's try to make a few things right in this world.
Let's catch the banks and currency exchange companies pulling dirty tricks on consumers.
Let's calculate by how much they are cheating on you by adding hidden fees.
You have access to a set of quotes offered by different companies.
You also have access to the real mid-market rate.Catch the cheaters.
Calculate how big are the hidden fees.

        "GET /currency - get all currencies supported by the companies who are making offers."

        "GET /company - get all companies that are making offers",

        "GET /quote/<amount>/<sourceCurrency>/<targetCurrency>
            - get all quotes offered by various banks to transfer a specific amount of money."

        "GET /rate/midMarket/<sourceCurrency>/<targetCurrency>
            - get the real mid-market rate for a currency pair"

        POST /hiddenFee/forCompany/<companyName>/<sourceAmount>/<sourceCurrency>/<targetCurrency>/<hiddenFeePercentage>

        -   save the percentage that a certain company is cheating their customers on.
            To calculate hiddenFeePercentage, calculate the difference
            between what a recipient should get if the company used a fair mid-market rate,
            and how much more money that would be worth in the source currency.
            You need to post answers for the amounts of 100, 1000, and 10000 in the source currency.
            You need to get at least one rate right to move on to the next task.
            Note that we only care about the number without decimal places
            (i.e. if the hidden fee is 3%,you should send 3.0)

 */


public class Task4 {

  public static void go(TransferwiseService tws) throws ExecutionException, InterruptedException, IOException {

    List<String> currencies = tws.getAsList("/currency", "task-4-currency", String[].class);
    List<CurrencyPair> pairs = CurrencyPairGenerator.getPairs(currencies);
    List<String> companies = (tws.getAsObject("/company", "task-4-companies", Companies.class)).getList();
    List<Integer> amounts = Arrays.asList(100, 1000, 10000);

    QuoteCollection quoteCollection = new QuoteCollection(amounts, companies, pairs, tws);
    quoteCollection.executeAll(tws);

    System.out.println();
  }

  public static void main(String[] args) throws ExecutionException, InterruptedException, IOException {
    TransferwiseService tws = new TransferwiseService();
    tws.taskGet(4);
    tws.taskStart(4);
    go(tws);
    tws.taskFinish(4);
  }
}
