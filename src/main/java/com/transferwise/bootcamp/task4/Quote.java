package com.transferwise.bootcamp.task4;

public class Quote {
  public Double commissionPercentage;
  public String targetCurrency;
  public Double feeInGbp;
  public Double youPay;
  public Double targetValue;
  public String sourceCurrency;
  public Double sourceAmount;
  public Double offerRate;
  public Double recipientReceives;
}
