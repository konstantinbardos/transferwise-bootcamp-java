package com.transferwise.bootcamp.task4;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by Qwosmic on 25/04/17.
 */
public class CurrencyPairGenerator {

  public static List<CurrencyPair> getPairs(List<String> currencies) {

    List<CurrencyPair> result = currencies
            .stream()
            .map(source ->
                    currencies
                            .stream()
                            .filter(target -> !target.equals(source))
                            .map(target -> new CurrencyPair(source, target)))

            .flatMap(Function.identity())
            .collect(Collectors.toList());

    return result;
  }
}
