package com.transferwise.bootcamp.task4;

/**
 * Created by Qwosmic on 25/04/17.
 */
public class CurrencyPair {
  public String source;
  public String target;

  public CurrencyPair(String source, String target) {
    this.source = source;
    this.target = target;
  }

  public CurrencyPair(String composite) {
    this.source = composite.substring(0, 3);
    this.target = composite.substring(3, 6);
  }

  @Override
  public String toString() {
    return String.format("%s%s", source, target);
  }

  @Override
  public boolean equals(Object obj) {
    CurrencyPair other = (CurrencyPair) obj;
    return this.source.equals(other.source) && this.target.equals(other.target);
  }
}
