package com.transferwise.bootcamp.task4;

import com.google.gson.internal.StringMap;
import com.google.gson.reflect.TypeToken;
import com.transferwise.bootcamp.TransferwiseService;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by Qwosmic on 25/04/17.
 */

public class QuoteCollection {

  private final List<Integer> amounts;
  private final List<String> companies;
  private final List<CurrencyPair> pairs;
  Map<Integer, Map<String, Map<String, Quote>>> quotes;
  Map<String, Double> rates;

  public QuoteCollection(List<Integer> amounts, List<String> companies, List<CurrencyPair> pairs, TransferwiseService tws)
          throws ExecutionException, InterruptedException {

    this.amounts = amounts;
    this.companies = companies;
    this.pairs = pairs;

    rates = pairs
            .stream()
            .parallel()
            .collect(Collectors
                    .toMap(pair -> pair.toString(),
                            pair -> loadMarketRate(pair, tws)));

    quotes =
            new ForkJoinPool(10).submit(() ->
                    amounts
                            .stream()
                            .parallel()
                            .collect(Collectors
                                    .toMap(
                                            Function.identity(),
                                            amount -> pairs
                                                    .stream()
                                                    .parallel()
                                                    .collect(Collectors.toMap(
                                                            pair -> pair.toString(),
                                                            pair -> loadQuotes(amount, pair.toString(), tws)
                                                    ))))).get();

  }

  public Map<String, Quote> loadQuotes(Integer amount, String pairString, TransferwiseService tws) {

    Type type = new TypeToken<Map<String, Quote>>() {
    }.getType();

    CurrencyPair pair = new CurrencyPair(pairString);

    return tws
            .getAsObject(
                    String.format("/quote/%d/%s/%s",
                            amount,
                            pair.source,
                            pair.target), null, type);
  }

  public Double loadMarketRate(CurrencyPair pair, TransferwiseService tws) {

    StringMap<Double> rate = tws.getAsStringMap(
            String.format("/rate/midMarket/%s/%s",
                    pair.source,
                    pair.target), null, Double.class);

    return rate.get("rate");
  }

  public Quote getQuote(Integer amount, CurrencyPair pair, String company) {
    return quotes
            .get(amount)
            .get(pair.toString())
            .get(company);
  }

  public Double getMarketRate(String source, String target) {
    return rates.get(new CurrencyPair(source, target).toString());
  }

  public String getHiddenFee(String company, Integer amount, CurrencyPair pair) {

    Quote quote = getQuote(amount, pair, company);
    double targetOffer = quote.targetValue;
    double targetMarket = amount * getMarketRate(pair.source, pair.target);
    double rateMarketInverse = getMarketRate(pair.target, pair.source);
    double hidden = ((targetMarket - targetOffer) * rateMarketInverse) / amount;

    return String.format("%.2g", Math.round(hidden * 100) + 0.0);
  }

  public String execute(String company, Integer amount, CurrencyPair pair, TransferwiseService tws) {

    String hidden = getHiddenFee(company, amount, pair);
    String endpoint = String
            .format(
                    "/hiddenFee/forCompany/%s/%s/%s/%s/%s",
                    company,
                    amount,
                    pair.source,
                    pair.target,
                    hidden);

    String jsonString = tws.post(endpoint);
    String msg = String.format("%30s --- executed --- %s", endpoint, Thread.currentThread().getName());
    System.out.println(msg);
    return jsonString;

  }

  public void executeAll(TransferwiseService tws) throws IOException {

    List<String> resultList;

    resultList = pairs
            .stream()
            .parallel()
            .map(pair -> companies
                    .stream()
                    .map(company -> amounts
                            .stream()
                            .map(amount -> execute(company, amount, pair, tws)))

                    .flatMap(Function.identity()))
            .flatMap(Function.identity())
            .collect(Collectors.toList());

    TransferwiseService.dumpList(resultList, "task-4-post");
  }
}
