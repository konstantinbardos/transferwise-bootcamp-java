package com.transferwise.bootcamp.task4;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Qwosmic on 25/04/17.
 */
public class Companies {
  String companies;
  public List<String> getList() {
    return Arrays.asList(companies.replaceAll("\\[|\\]|,", "").split(" "));
  }
}
