package com.transferwise.bootcamp.task5;

/*

0 = {StringMap$LinkedEntry@1752} "id" -> "58ffb03285e4c007812948c3"
1 = {StringMap$LinkedEntry@1753} "email" -> "null"
2 = {StringMap$LinkedEntry@1754} "amount" -> "33.13"
3 = {StringMap$LinkedEntry@1755} "sourceCurrency" -> "USD"
4 = {StringMap$LinkedEntry@1756} "targetCurrency" -> "INR"
5 = {StringMap$LinkedEntry@1757} "recipientName" -> "Anastasia Volkov"
6 = {StringMap$LinkedEntry@1758} "recipientCountry" -> "Korea, Democratic People's Republic of"
7 = {StringMap$LinkedEntry@1759} "type" -> "null"
8 = {StringMap$LinkedEntry@1760} "iban" -> "3042764358"
9 = {StringMap$LinkedEntry@1761} "ip" -> "null"
10 = {StringMap$LinkedEntry@1762} "created" -> "2017-04-25T20:23:14.131"

 */

public class Payment {
  public String id;
  public String email;
  public Double amount;
  public String sourceCurrency;
  public String targetCurrency;
  public String recipientName;
  public String recipientCountry;
  public String type;
  public String iban;
  public String ip;
  public String created;
}
