package com.transferwise.bootcamp.task5;

import com.google.gson.internal.StringMap;
import com.transferwise.bootcamp.TransferwiseService;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by Qwosmic on 25/04/17.
 */

public class PepCollection {

  Map<String, String> pepMap;

  public PepCollection(TransferwiseService tws) {

    StringMap<Object> task = tws.getAsStringMap("/task/5", null, Object.class);

    pepMap = ((List<String>) task.get("peps"))
            .stream()
            .map(Pep::new)
            .collect(Collectors
                    .toMap(
                            pep -> pep.name,
                            pep -> pep.contry));

  }

  public Boolean contains(Payment payment) {
    String country = pepMap.get(payment.recipientName);
    if (country == null) {
      return false;
    } else {
      return country.equals(payment.recipientCountry);
    }
  }
}
