package com.transferwise.bootcamp.task5;

/**
 * Created by Qwosmic on 25/04/17.
 */
public class Pep {
  public String name;
  public String contry;

  public Pep(String string) {
    String[] arr = string.split(" - ");
    if (arr.length == 2) {
      name = arr[0];
      contry = arr[1];
    } else {
      throw new RuntimeException("Incorrect Pep creation string");
    }
  }
}
