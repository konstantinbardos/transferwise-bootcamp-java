package com.transferwise.bootcamp.task3;

/*

  "id":"58fd09ad85e4c0078129121b",
  "accountNumber":"6617770448",
  "accountName":"TransferWise Ltd",
  "bankId":"58fd09ad85e4c0078129121a",
  "currency":"EUR",
  "balance":10000000},

 */


public class BankAccount {
  public String id;
  public String accountNumber;
  public String accountName;
  public String bankId;
  public String currency;
  public Double balance;

}
