package com.transferwise.bootcamp.task3;

/*

  "id" : "58fd046d85e4c00781290cdf",
  "amount" : 93306.54,
  "sourceCurrency" : "EUR",
  "targetCurrency" : "MXN",
  "recipientBankId" : "58fd046d85e4c00781290cd8",
  "iban" : "7613384707",
  "created" : "2017-04-23T19:45:49.147"

 */


import com.transferwise.bootcamp.TransferwiseService;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;

public class Payment {

  public String id;
  public Double amount;
  public String sourceCurrency;
  public String targetCurrency;
  public String recipientBankId;
  public String iban;
  public String created;

  public String execute(Map<String, String> bankMap, List<BankAccount> bankAccounts) {

    BankAccount src = getSourceAccount(sourceCurrency, amount, bankAccounts);
    String sourceBankName = bankMap.get(src.bankId);
    String sourceAccount = src.accountNumber;
    String targetBankName = bankMap.get(recipientBankId);

    TransferwiseService tws = new TransferwiseService();
    try {

      String endpoint = String.format("/bank/%s/transfer/%s/%s/%s/%s",
              URLEncoder.encode(sourceBankName, "UTF-8").replace("+", "%20"),
              sourceAccount,
              URLEncoder.encode(targetBankName, "UTF-8").replace("+", "%20"),
              iban,
              amount.toString());

      String jsonString = tws.post(endpoint);
      String msg = Thread.currentThread().getName() + " is executing --- " + endpoint + " --- " + jsonString + "\n";
      System.out.print(msg);
      return msg;

    } catch (UnsupportedEncodingException ex) {
      throw new RuntimeException("Houston, we have a problem: " + ex.getMessage());
    }
  }

  static BankAccount getSourceAccount(String currency, Double amount, List<BankAccount> l) {
    BankAccount bankAccount = l
            .stream()
            .filter(account ->
                    account.currency.equals(currency) &&
                            account.accountName.equals("TransferWise Ltd") &&
                            account.balance >= amount)
            .findFirst().get();

    return bankAccount;
  }

}
