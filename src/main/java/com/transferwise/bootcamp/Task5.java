package com.transferwise.bootcamp;

import com.transferwise.bootcamp.task5.Payment;
import com.transferwise.bootcamp.task5.PepCollection;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import static com.transferwise.bootcamp.TransferwiseService.*;

/*

    Task 5

    Now that we have the basics covered, it's time to look at how we can
    build a system that follows the rules that government regulators
    have in different countries. For this purpose, we should check
    if the person receiving the money is somebody who holds power
    in a political office - also known as a Politically Exposed Person (PEP).
    You have available a list of the people who should be marked as PEP.

        "GET /payment - get payments that should be checked before we process them",
        "PUT /payment/{id}/aml - to mark transaction recipient as 'politically exposed person' recipient",
        "DELETE /payment/{id}/aml - to mark transaction recipient as not 'politically exposed person' recipient"

 */

public class Task5 {

  static String processPayment(Payment payment, PepCollection pepCollection, TransferwiseService tws) {

    if (pepCollection.contains(payment)) {
      return tws
              .generalRequest(
                      RequestType.PUT,
                      String.format("/payment/%s/aml", payment.id),
                      null);
    } else {
      return tws
              .generalRequest(
                      RequestType.DELETE,
                      String.format("/payment/%s/aml", payment.id),
                      null);
    }
  }

  public static void go(TransferwiseService tws) throws IOException {

    PepCollection pepCollection = new PepCollection(tws);
    List<Payment> payments = tws.getAsList("/payment", "task-5-payments", Payment[].class);

    List<String> results =
            payments
                    .stream()
                    .parallel()
                    .map(payment -> processPayment(payment, pepCollection, tws))
                    .collect(Collectors.toList());

    TransferwiseService.dumpList(results, "task-5-post");
  }

  public static void main(String[] args) throws ExecutionException, InterruptedException, IOException {
    TransferwiseService tws = new TransferwiseService();
    tws.taskGet(5);
    tws.taskStart(5);
    go(tws);
    tws.taskFinish(5);
  }
}