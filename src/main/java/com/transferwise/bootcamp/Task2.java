package com.transferwise.bootcamp;

/*

 Task 2:

 Take a second to imagine that you are in a room with 100 chairs arranged in a circle.

 These chairs are numbered sequentially from 1 to 100.
 At some point in time, the person in chair #1 will be removed from the room along with his chair.
 The person in chair #2 will be skipped, and the person in chair #3 will be told to leave.

 Next to go is person in chair #6.
 In other words, 1 person will be skipped initially, and then 2, 3, 4.. and so on.
 This pattern of skipping will keep going around the circle until there is only one person remaining...
 You have to figure out which is the number of the last remaining chair. Who survives this Game of Chairs?"

 */


import com.transferwise.bootcamp.task2.SurvivorGame;

public class Task2 {


  public static void main(String[] args) {

    TransferwiseService tws = new TransferwiseService();

    SurvivorGame game = new SurvivorGame(100);
    Integer survivor = game.getSurvivor();

    tws.taskGet(2);
    tws.taskStart(2);
    tws.post("/survivor/" + game.getSurvivor(), "task-2-post");
    tws.taskFinish(2);
  }
}
