package com.transferwise.bootcamp;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * Created by Qwosmic on 23/04/17.
 */
public class Task1 {

  public static void main(String[] args) throws UnsupportedEncodingException {
    TransferwiseService tws = new TransferwiseService();

    tws.taskGet(1);
    tws.taskStart(1);

    tws.get("/name", "task-1-name-get");
    tws.post("/name/" + URLEncoder.encode("Konstantin Bardos", "UTF-8"), "task-1-name-post");

    tws.taskFinish(1);
  }
}
