package com.transferwise.bootcamp.task6;

import com.transferwise.bootcamp.TransferwiseService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.transferwise.bootcamp.TransferwiseService.RequestType.*;

/*

 */

public class Detector {

  Set<String> ipMasks;

  public Detector(List<HistoryPayment> history) {
    ipMasks = history
            .stream()
            .filter(payment -> payment.fraud)
            .map(payment -> new IpAddress(payment.ip))
            .map(addr -> addr.mask)
            .collect(Collectors.toSet());
  }

  private boolean isFraud(Payment payment){
    String ipMask = new IpAddress(payment.ip).mask;
    return ipMasks.contains(ipMask);
  }

  public String detectAndProcess(Payment payment, TransferwiseService tws) {
    String endpoint = String.format("/payment/%s/fraud", payment.id);
    if (isFraud(payment)) {
      return tws.generalRequest(PUT, endpoint, null);
    } else {
      return tws.generalRequest(DELETE, endpoint, null);
    }
  }
}
