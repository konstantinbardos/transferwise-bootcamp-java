package com.transferwise.bootcamp.task6;

public class GeneralPayment {

  public String email;
  public Double amount;
  public String sourceCurrency;
  public String targetCurrency;
  public String recipientName;
  public String type;
  public String iban;
  public String ip;
  public String created;

}
