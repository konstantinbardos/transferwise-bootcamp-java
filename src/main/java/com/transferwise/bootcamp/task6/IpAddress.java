package com.transferwise.bootcamp.task6;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Qwosmic on 26/04/17.
 */

public class IpAddress {
  public String stringIp;
  public String mask;

  public IpAddress(String stringIp) {
    this.stringIp = stringIp;
    List<String> list = Arrays.asList(stringIp.split("\\."));
    if (list.size() != 4) {
      throw new RuntimeException("Trying to create incorrect IP address");
    } else {
      mask = list
              .stream()
              .limit(3)
              .collect(Collectors.joining("."));
    }
  }
}
