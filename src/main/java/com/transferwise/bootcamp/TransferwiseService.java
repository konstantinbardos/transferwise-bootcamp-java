package com.transferwise.bootcamp;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.internal.StringMap;
import com.google.gson.reflect.TypeToken;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.utils.URIBuilder;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

public class TransferwiseService {

  static String URL = "http://54.229.242.6";
  static String TOKEN = "36db9a2e40f3f1754fa7baae212ce836";
  static String DUMP_PATH = "./dump/";

  Gson gsonBuilder = new GsonBuilder().disableHtmlEscaping().setPrettyPrinting().create();
  Gson gson = new Gson();

  public enum RequestType {
    GET, POST, PUT, DELETE
  }

  public String toPrettyString(String string) {
    JsonParser parser = new JsonParser();
    try {
      JsonObject json = parser.parse(string).getAsJsonObject();
      return gsonBuilder.toJson(json);
    } catch (IllegalStateException ignore) {
      return string;
    }
  }

  static void dumpString(String json, String filename) throws IOException {
    System.out.println("============================================================================");
    System.out.println(filename + ": ");
    System.out.println("----------------------------------------------------------------------------");
    System.out.println(json);
    Files.write(Paths.get(DUMP_PATH + filename + ".json"), json.getBytes(Charset.forName("UTF-8")));
  }

  public static void dumpList(List<String> data, String filename) throws IOException {
    StringBuilder sb = new StringBuilder();
    for (String elem : data) {
      sb.append(elem);
    }
    Files.write(Paths.get(DUMP_PATH + filename + ".json"), sb.toString().getBytes(Charset.forName("UTF-8")));
  }

  public String generalRequest(RequestType requestType, String endpoint, String requestName) {
    String json = null;
    try {
      URI uri = new URIBuilder(URL + endpoint).setParameter("token", TOKEN).build();

      switch (requestType) {
        case GET:
          json = Request.Get(uri).execute().returnContent().asString();
          break;
        case POST:
          json = Request.Post(uri).execute().returnContent().asString();
          break;
        case PUT:
          json = Request.Put(uri).execute().returnContent().asString();
          break;
        case DELETE:
          json = Request.Delete(uri).execute().returnContent().asString();
          break;
      }

      if (requestName != null) {
        dumpString(json, requestName);
      }

    } catch (IOException | URISyntaxException e) {
      e.printStackTrace();
    }

    String msg = String.format("%30s --- executed --- %s", endpoint, Thread.currentThread().getName());
    System.out.println(msg);
    return json;
  }

  public String get(String endpoint, String requestName) {
    String result = generalRequest(RequestType.GET, endpoint, requestName);
    return toPrettyString(result);
  }

  public String post(String endpoint, String requestName) {
    return generalRequest(RequestType.POST, endpoint, requestName);
  }

  public String post(String endpoint) {
    return post(endpoint, null);
  }

  public <T> List<T> getAsList(String endpoint, String requestName, Class<T[]> clazz) {
    String jsonString = get(endpoint, requestName);
    T[] jsonObject = new Gson().fromJson(jsonString, clazz);
    return Arrays.asList(jsonObject);
  }

  public <T> T getAsObject(String endpoint, String requestName, Class<T> clazz) {
    String jsonString = get(endpoint, requestName);
    T jsonObject = new Gson().fromJson(jsonString, clazz);
    return jsonObject;
  }

  public <T> T getAsObject(String endpoint, String requestName, Type type) {
    String jsonString = get(endpoint, requestName);
    T jsonObject = new Gson().fromJson(jsonString, type);
    return jsonObject;
  }

  public <T> StringMap<T> getAsStringMap(String endpoint, String requestName, Class<T> clazz) {
    Type type = new TypeToken<StringMap<T>>() {
    }.getType();
    return getAsObject(endpoint, requestName, type);
  }

  public String taskGet(int id) {
    return get("/task/" + id, "task-" + id + "-get");
  }

  public String taskStart(int id) {
    return post("/task/" + id + "/start", "task-" + id + "-start");
  }

  public String taskFinish(int id) {
    return post("/task/finish", "task-" + id + "-finish");
  }
}