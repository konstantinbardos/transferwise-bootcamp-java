package com.transferwise.bootcamp;

import com.transferwise.bootcamp.task3.Bank;
import com.transferwise.bootcamp.task3.BankAccount;
import com.transferwise.bootcamp.task3.Payment;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ForkJoinPool;
import java.util.stream.Collectors;

public class Task3 {

  public static void go(TransferwiseService tws) throws Exception {

    final List<Bank> banks = tws.getAsList("/bank", "task-3-bank", Bank[].class);
    final List<BankAccount> bankAccounts = tws.getAsList("/bankAccount", "task-3-bankAccounts", BankAccount[].class);
    final List<Payment> payments = tws.getAsList("/payment", "task-3-payments", Payment[].class);

    Map<String, String> bankMap = banks
            .stream()
            .collect(Collectors
                    .toMap(bank -> bank.id, bank -> bank.name));

    List<String> postResults =
            new ForkJoinPool(10)
                    .submit(() -> payments
                            .stream()
                            .parallel()
                            .map(payment -> payment.execute(bankMap, bankAccounts))
                            .collect(Collectors.toList())).get();


    TransferwiseService.dumpList(postResults, "task-3-post");
    System.out.println("Done...");

  }

  public static void main(String[] args) throws Exception {
    TransferwiseService tws = new TransferwiseService();
    tws.taskGet(3);
    tws.taskStart(3);
    go(tws);
    tws.taskFinish(3);
  }
}
