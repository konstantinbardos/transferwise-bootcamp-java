package com.transferwise.bootcamp.task2;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by Qwosmic on 23/04/17.
 */

public class SurvivorGame {
  private List<Integer> personList;
  private Integer currentPosition = 0;
  private Integer toSkip = 0;

  public SurvivorGame(Integer numOfPeople) {
    if (numOfPeople < 1) {
      throw new RuntimeException("Cannot start game with " + numOfPeople + " persons");
    }
    personList = IntStream.range(1, numOfPeople + 1).boxed().collect(Collectors.toList());
  }

  public int getNextPosition() {
    currentPosition = (currentPosition + toSkip) % personList.size();
    toSkip++;
    return currentPosition;
  }

  public Integer getNextPerson() {
    if (isActive()) {
      return personList.remove(getNextPosition());
    }
    return personList.get(0);
  }

  public Boolean isActive() {
    return personList.size() > 1;
  }

  public Integer getSurvivor() {
    while (isActive()) getNextPerson();
    return getNextPerson();
  }
}
