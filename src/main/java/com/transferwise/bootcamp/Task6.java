package com.transferwise.bootcamp;

/*

    Task 6

    You are sailing along nicely. However, don't let your guards down.
    For any payments system, there are bound to be shady people wanting to take advantage.
    These people are trying to steal money from unwitting victims by stealing
    their credit card numbers and bank account access.
    Your help is needed to analyze our payment history and find some patterns for fraudulent payments.
    We'll provide you a sample of our payments with 10 fraudsters in it that we successfully caught.
    It's not over yet, though - 100 more payments are coming in, and waiting for your justice!
    But keep in mind that if you mark a good payment as fraudulent,
    it will be as damaging as catching a real fraudster.

            "GET /payment/history - get the past payments that have been made by confirmed fraudsters",
            "GET /payment - get payments that should be checked before we process them",
            "PUT /payment/{id}/fraud - to mark a transaction as fraud",
            "DELETE /payment/{id}/fraud - to mark a transaction as non-fraud"

 */


import com.transferwise.bootcamp.task6.Detector;
import com.transferwise.bootcamp.task6.HistoryPayment;
import com.transferwise.bootcamp.task6.Payment;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

public class Task6 {

  private static void go(TransferwiseService tws) throws IOException {

    List<HistoryPayment> historyPaymentList = tws.getAsList("/payment/history", "task-6-history", HistoryPayment[].class);
    List<Payment> paymentList = tws.getAsList("/payment", "task-6-payment", Payment[].class);

    Detector detector = new Detector(historyPaymentList);

    List<String> paymentResults = paymentList
            .stream()
            .parallel()
            .map(payment -> detector.detectAndProcess(payment, tws))
            .collect(Collectors.toList());

    TransferwiseService.dumpList(paymentResults, "task-6-post");
  }

  public static void main(String[] args) throws IOException {
    TransferwiseService tws = new TransferwiseService();

    tws.taskGet(6);
    tws.taskStart(6);
    go(tws);
    tws.taskFinish(6);
  }
}
